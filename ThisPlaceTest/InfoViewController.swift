//
//  InfoViewController.swift
//  ThisPlaceTest
//
//  Created by Number Two on 6/12/17.
//  Copyright © 2017 Senecio. All rights reserved.
//

import UIKit


class InfoController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var InfoTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.InfoTableView.dataSource = self
        self.InfoTableView.delegate = self
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "Info Cell", for: indexPath) as! TableViewCell
        return cell
     
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 4800
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
