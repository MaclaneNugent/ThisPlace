//
//  SettingsViewController.swift
//  ThisPlace
//
//  Created by Number One on 3/25/17.
//  Copyright © 2017 Number Three. All rights reserved.
//

import Foundation
import UIKit

class SettingsViewController : UIViewController
{
    @IBOutlet weak var lblTheme: UILabel!
    @IBOutlet weak var ThemeSC: UISegmentedControl!
    var theme = 0
    
    @IBAction func ThemeSCChanged(_ sender: UISegmentedControl) {
        theme = ThemeSC.selectedSegmentIndex
        UserDefaults.standard.set(theme, forKey: "theme")
        
        if theme == 1 {
            view.backgroundColor = UIColor.black
            lblTheme.textColor = UIColor.white
        }
        else {
            view.backgroundColor = UIColor.white
            lblTheme.textColor = UIColor.black
        }
    }
    override func viewDidLoad() {
        
        theme = UserDefaults.standard.integer(forKey: "theme")
        if theme == 1 {
            ThemeSC.selectedSegmentIndex = 1
            view.backgroundColor = UIColor.black
            lblTheme.textColor = UIColor.white
        }
        else {
            ThemeSC.selectedSegmentIndex = 0
            view.backgroundColor = UIColor.white
            lblTheme.textColor = UIColor.black
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let _destination = segue.destination as? ViewController{
            _destination.themeNo = theme
        }
    }
    
    
}
