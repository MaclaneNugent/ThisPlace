//
//  ViewController.swift
//  ThisPlaceTest
//
//  Created by Number Two on 5/19/17.
//  Copyright © 2017 Senecio. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreLocation
import MapKit
import Charts
import FileExplorer
import PDFGenerator

class ViewController: UIViewController, CLLocationManagerDelegate, UITableViewDelegate, UITableViewDataSource{
    
    // Buttons and Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var generate: UIButton!
    // Buttons and Outlets End
    
    
    // Variables
    // Location
    var lat: Double = 0
    var lng: Double = 0
    var alt: Double = 0
    
    // Population
    var totalPop: Double = 0
    var ethnicity_HisOrLat: Double = 0
    var ethnicity_Not_HisOrLat: Double = 0
    var race_white: Double = 0
    var race_AfricanAmerican: Double = 0
    var race_Asian: Double = 0
    var race_AIAN: Double = 0
    var race_NHPI: Double = 0
    var race_SOR: Double = 0
    var race_ToMR: Double = 0
    var age_Male: Double = 0
    var age_Female: Double = 0
    var age_18Under: Double = 0
    var age_20to24: Double = 0
    var age_25to34: Double = 0
    var age_35to49: Double = 0
    var age_50to64: Double = 0
    var age_65Above: Double = 0
    var age_18to64: Double = 0
    
    // Housing
    var housing_total: Double = 0
    var housing_Occupied: Double = 0
    var housing_Owner_OccupiedTOTAL: Double = 0
    var housing_Owner_PopulationTOTAL: Double = 0
    var housing_Renter_Occupied: Double = 0
    var housing_Renter_Population: Double = 0
    var housing_withMinorsTOTAL: Double = 0
    var housing_Vacant: Double = 0
    var housing_Vacant_ForRent: Double = 0
    var housing_Vacant_ForSale: Double = 0
    var housing_Seasonal: Double = 0
    
    // Closest Address
    var addressVar: String = "Label"
    var cityVar: String = "Label"
    var stateVar: String = "Label"
    var zipVar: String = "Label"
    var nearAddressVar: String = "Label"
    
    // Location Manager Variables
    var lastLocation: CLLocationCoordinate2D? = nil
    let locationManager = CLLocationManager()
   
    // Chart Values
    var ppyMaleUnder5: Double = 0
    var ppyMale5to9: Double = 0
    var ppyMale10to14: Double = 0
    var ppyMale15to19: Double = 0
    var ppyMale20to24: Double = 0
    var ppyMale25to29: Double = 0
    var ppyMale30to34: Double = 0
    var ppyMale35to39: Double = 0
    var ppyMale40to44: Double = 0
    var ppyMale45to49: Double = 0
    var ppyMale50to54: Double = 0
    var ppyMale55to59: Double = 0
    var ppyMale60to64: Double = 0
    var ppyMale65to69: Double = 0
    var ppyMale70to74: Double = 0
    var ppyMale75to79: Double = 0
    var ppyMale80to84: Double = 0
    var ppyMaleAbove85: Double = 0
    var ppyFemaleUnder5: Double = 0
    var ppyFemale5to9: Double = 0
    var ppyFemale10to14: Double = 0
    var ppyFemale15to19: Double = 0
    var ppyFemale20to24: Double = 0
    var ppyFemale25to29: Double = 0
    var ppyFemale30to34: Double = 0
    var ppyFemale35to39: Double = 0
    var ppyFemale40to44: Double = 0
    var ppyFemale45to49: Double = 0
    var ppyFemale50to54: Double = 0
    var ppyFemale55to59: Double = 0
    var ppyFemale60to64: Double = 0
    var ppyFemale65to69: Double = 0
    var ppyFemale70to74: Double = 0
    var ppyFemale75to79: Double = 0
    var ppyFemale80to84: Double = 0
    var ppyFemaleAbove85: Double = 0
    
    // census variables
    var countyVar: String = "Test"
    var tractVar: String = "Test"
    
    // Holds all data
    var dataAry = [Double]()
    
    // theme
    var themeNo = 0
    
    // Main
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Hello World")
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        //self.tableView.frame = self.view.bounds
        //self.tableView.scalesPageToFit = true
        
        self.locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        themeNo = UserDefaults.standard.integer(forKey: "theme")
        
        if themeNo == 1 {
            view.backgroundColor = UIColor.black
            UITableViewCell.appearance().backgroundColor = UIColor.black
            UILabel.appearance().textColor = UIColor.white
        }
        else {
            view.backgroundColor = UIColor.white
            UITableViewCell.appearance().backgroundColor = UIColor.white
            UILabel.appearance().textColor = UIColor.black
        }
        someButtonClicked(sender: generate )
        let timer = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false) { (timer) in
            // do stuff 42 seconds later
            self.someButtonClicked(sender: self.generate )
           // self.tableView.reloadData()
        }
        
        someButtonClicked(sender: generate )
        generate.addTarget(self, action: #selector(ViewController.someButtonClicked(sender:)), for: .touchUpInside)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }// Main End
    
    // Button Function
    func someButtonClicked(sender: UIButton){
        
        let defaults = UserDefaults.standard
        
        // LOCATION
        if ((lastLocation?.latitude) != nil) && (lastLocation?.longitude != nil) {
            lat = (lastLocation?.latitude)!
            lng = (lastLocation?.longitude)!
        }
        else
        {
            lat = 40.759211
            lng = -73.984638
        }
        
        //alt = lastLocation?.altitude
        
        //Variables for the API request
        let apikey : String = "442f76259b9e59c8eec188611fd43ab3d01310a5"
        let request_url : String = "http://citysdk.commerce.gov"
        let passWord : String = ""
        
        // Parameters for API request
        let parameters: Parameters = [
            "level": "tract",
            "lat": lat,
            "lng": lng,
            "variables": ["P0040001", "P0040002", "P0040003", "P0030002", "P0030003", "P0030005", "P0030004", "P0030006", "P0030007", "P0030008", "P0120002", "P0120026", "P0120003", "P0120004", "P0120005", "P0120006", "P0120027", "P0120028", "P0120029", "P0120030", "P0120008", "P0120009", "P0120010", "P0120032", "P0120033", "P0120034", "P0120011", "P0120012", "P0120035", "P0120036", "P0120013", "P0120014", "P0120015", "P0120037", "P0120038", "P0120039", "P0120016", "P0120017", "P0120018", "P0120019", "P0120040", "P0120041", "P0120042", "P0120043"],
            "api": "sf1",
            "year": "2010"
        ]
        
        // Parameters for API request 2
        let parameters2: Parameters = [
            "level": "tract",
            "lat": lat,
            "lng": lng,
            "variables": ["P0120020", "P0120021", "P0120022", "P0120023", "P0120024", "P0120025", "P0120044", "P0120045", "P0120046", "P0120047", "P0120048", "P0120049","H0040004", "H0110004", "H00010001", "H0180001", "H0040002", "H0040003", "H0110002", "H0110003", "H0190003", "H0190006", "H0050001", "H0050002", "H0020004", "H0050006", "P012A007", "P012A031"],
            "api": "sf1",
            "year": "2010"
        ]
        
        
        // Header to authenticate the API key
        var headers: HTTPHeaders = [
            "Authorization": "Basic 442f76259b9e59c8eec188611fd43ab3d01310a5",
            "Accept": "application/json"
        ]
        
        // Authorization request
        if let authorizationHeader = Request.authorizationHeader(user: apikey, password: passWord){
            headers[authorizationHeader.key] = authorizationHeader.value
        }
        
        
        if lastLocation != nil {
            Alamofire.request(request_url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON {
                response in switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print(json)
                    print()
                    
                    //=================
                    // POPULATION
                    //=================
                    
                    // total population
                    self.totalPop = json["features"][0]["properties"]["P0040001"].doubleValue
                    self.dataAry.append(self.totalPop)
                    
                    //=================
                    // ETHNICITY
                    //=================
                    
                    // Hispanic or Latino
                    self.ethnicity_HisOrLat = json["features"][0]["properties"]["P0040003"].doubleValue
                    self.dataAry.append(self.ethnicity_HisOrLat)
                    
                    // Not Hispanic or Latino
                    self.ethnicity_Not_HisOrLat = json["features"][0]["properties"]["P0040002"].doubleValue
                    self.dataAry.append(self.ethnicity_Not_HisOrLat)
                    
                    //=================
                    // RACE
                    //=================
                    
                    // White
                    self.race_white = json["features"][0]["properties"]["P0030002"].doubleValue
                    self.dataAry.append(self.race_white)
                    
                    // African American
                    self.race_AfricanAmerican = json["features"][0]["properties"]["P0030003"].doubleValue
                    self.dataAry.append(self.race_AfricanAmerican)
                    
                    // Asian
                    self.race_Asian = json["features"][0]["properties"]["P0030005"].doubleValue
                    self.dataAry.append(self.race_Asian)
                    
                    // AIAN
                    self.race_AIAN = json["features"][0]["properties"]["P0030004"].doubleValue
                    self.dataAry.append(self.race_AIAN)
                    
                    // NHPI
                    self.race_NHPI = json["features"][0]["properties"]["P0030006"].doubleValue
                    self.dataAry.append(self.race_NHPI)
                    
                    // SOR
                    self.race_SOR = json["features"][0]["properties"]["P0030007"].doubleValue
                    self.dataAry.append(self.race_SOR)
                    
                    // ToMR
                    self.race_ToMR = json["features"][0]["properties"]["P0030008"].doubleValue
                    self.dataAry.append(self.race_ToMR)
                    
                    //=================
                    // Age
                    //=================
                    
                    // Male
                    self.age_Male = json["features"][0]["properties"]["P0120002"].doubleValue
                    self.dataAry.append(self.age_Male)
                    
                    // Female
                    self.age_Female = json["features"][0]["properties"]["P0120026"].doubleValue
                    self.dataAry.append(self.age_Female)
                    
                    // Males under 18
                    let age_Male_underFive = json["features"][0]["properties"]["P0120003"].doubleValue
                    let age_Male_5to9 = json["features"][0]["properties"]["P0120004"].doubleValue
                    let age_Male_10to14 = json["features"][0]["properties"]["P0120005"].doubleValue
                    let age_Male_15to17 = json["features"][0]["properties"]["P0120006"].doubleValue
                    let age_Male_18to19 = json["features"][0]["properties"]["P012A007"].doubleValue
                    self.ppyMaleUnder5 = age_Male_underFive
                    self.ppyMale5to9 = age_Male_5to9
                    self.ppyMale10to14 = age_Male_10to14
                    self.ppyMale15to19 = age_Male_15to17 + age_Male_18to19
                    self.dataAry.append(self.ppyMaleUnder5)
                    self.dataAry.append(self.ppyMale5to9)
                    self.dataAry.append(self.ppyMale10to14)
                    self.dataAry.append(self.ppyMale15to19)
                    
                    // variable and for-in loop for male under 18 total
                    var male_Under18 = 0.0
                    for index in 3...6{
                        male_Under18 += json["features"][0]["properties"]["P012000\(index)"].doubleValue
                    }
                    
                    // Females under 18
                    let age_Female_underFive = json["features"][0]["properties"]["P0120027"].doubleValue
                    let age_Female_5to9 = json["features"][0]["properties"]["P0120028"].doubleValue
                    let age_Female_10to14 = json["features"][0]["properties"]["P0120029"].doubleValue
                    let age_Female_15to17 = json["features"][0]["properties"]["P0120030"].doubleValue
                    let age_Female_18to19 = json["features"][0]["properties"]["P012A031"].doubleValue
                    self.ppyFemaleUnder5 = age_Female_underFive
                    self.ppyFemale5to9 = age_Female_5to9
                    self.ppyFemale10to14 = age_Female_10to14
                    self.ppyFemale15to19 = age_Female_15to17 + age_Female_18to19
                    self.dataAry.append(self.ppyFemaleUnder5)
                    self.dataAry.append(self.ppyFemale5to9)
                    self.dataAry.append(self.ppyFemale10to14)
                    self.dataAry.append(self.ppyFemale15to19)
                    
                    print("BUTTON F15-17: \(age_Female_15to17)")
                    print("BUTTON F18-19: \(age_Female_18to19)")
                    
                    // variable and for-in loop for female under 18 total
                    var female_Under18 = 0.0
                    for index in 27...30{
                        female_Under18 += json["features"][0]["properties"]["P01200\(index)"].doubleValue
                    }
                    
                    // Total Under 18
                    self.age_18Under = male_Under18 + female_Under18
                    
                    // Male 20-24
                    let age_Male_20 = json["features"][0]["properties"]["P0120008"].doubleValue
                    let age_Male_21 = json["features"][0]["properties"]["P0120009"].doubleValue
                    let age_Male_22to24 = json["features"][0]["properties"]["P0120010"].doubleValue
                    self.ppyMale20to24 = age_Male_20 + age_Male_21 + age_Male_22to24
                    self.dataAry.append(self.ppyMale20to24)
                    
                    // variable and for-in loop for male 20 to 24 total
                    var male_20to24 = 0.0
                    for index in 8...9{
                        male_20to24 += json["features"][0]["properties"]["P012000\(index)"].doubleValue
                    }
                    male_20to24 += json["features"][0]["properties"]["P0120010"].doubleValue
                    
                    // Female 20-24
                    let age_Female_20 = json["features"][0]["properties"]["P0120032"].doubleValue
                    let age_Female_21 = json["features"][0]["properties"]["P0120033"].doubleValue
                    let age_Female_22to24 = json["features"][0]["properties"]["P0120034"].doubleValue
                    self.ppyFemale20to24 = age_Female_20 + age_Female_21 + age_Female_22to24
                    self.dataAry.append(self.ppyFemale20to24)
                    
                    // variable and for-in loop for female 20 to 24 total
                    var female_20to24 = 0.0
                    for index in 32...34{
                        female_20to24 += json["features"][0]["properties"]["P01200\(index)"].doubleValue
                    }
                    
                    // Total 20-24
                    self.age_20to24 = Double(male_20to24 + female_20to24)
                    
                    // Male 25-34
                    let age_Male_25to29 = json["features"][0]["properties"]["P0120011"].doubleValue
                    let age_Male_30to34 = json["features"][0]["properties"]["P0120012"].doubleValue
                    let male_25to34 = (age_Male_25to29 + age_Male_30to34)
                    self.ppyMale25to29 = age_Male_25to29
                    self.ppyMale30to34 = age_Male_30to34
                    self.dataAry.append(self.ppyMale25to29)
                    self.dataAry.append(self.ppyMale30to34)
                    
                    // Female 25-34
                    let age_Female_25to29 = json["features"][0]["properties"]["P0120035"].doubleValue
                    let age_Female_30to34 = json["features"][0]["properties"]["P0120036"].doubleValue
                    let female_25to34 = (age_Female_25to29 + age_Female_30to34)
                    self.ppyFemale25to29 = age_Female_25to29
                    self.ppyFemale30to34 = age_Female_30to34
                    self.dataAry.append(self.ppyFemale25to29)
                    self.dataAry.append(self.ppyFemale30to34)
                    
                    // Total 25-34
                    self.age_25to34 = Double(male_25to34 + female_25to34)
                    
                    // Males 35-49
                    let age_Male_35to39 = json["features"][0]["properties"]["P0120013"].doubleValue
                    let age_Male_40to44 = json["features"][0]["properties"]["P0120014"].doubleValue
                    let age_Male_45to49 = json["features"][0]["properties"]["P0120015"].doubleValue
                    self.ppyMale35to39 = age_Male_35to39
                    self.ppyMale40to44 = age_Male_40to44
                    self.ppyMale45to49 = age_Male_45to49
                    self.dataAry.append(self.ppyMale35to39)
                    self.dataAry.append(self.ppyMale40to44)
                    self.dataAry.append(self.ppyMale45to49)
                    
                    // variable and for-in loop for male 35-49 total
                    var male_35to49 = 0.0
                    for index in 13...15{
                        male_35to49 += json["features"][0]["properties"]["P01200\(index)"].doubleValue
                    }
                    
                    // Females 35-49
                    let age_Female_35to39 = json["features"][0]["properties"]["P0120027"].doubleValue
                    let age_Female_40to44 = json["features"][0]["properties"]["P0120028"].doubleValue
                    let age_Female_45to49 = json["features"][0]["properties"]["P0120029"].doubleValue
                    self.ppyFemale35to39 = age_Female_35to39
                    self.ppyFemale40to44 = age_Female_40to44
                    self.ppyFemale45to49 = age_Female_45to49
                    self.dataAry.append(self.ppyFemale35to39)
                    self.dataAry.append(self.ppyFemale40to44)
                    self.dataAry.append(self.ppyFemale45to49)
                    
                    // variable and for-in loop for female 35-49 total
                    var female_35to49 = 0.0
                    for index in 37...39{
                        female_35to49 += json["features"][0]["properties"]["P01200\(index)"].doubleValue
                    }
                    
                    // Total 35-49
                    self.age_35to49 = male_35to49 + female_35to49
                    
                    
                    // Males 50-64
                    let age_Male_50to54 = json["features"][0]["properties"]["P0120016"].doubleValue
                    let age_Male_55to59 = json["features"][0]["properties"]["P0120017"].doubleValue
                    let age_Male_60to61 = json["features"][0]["properties"]["P0120018"].doubleValue
                    let age_Male_62to64 = json["features"][0]["properties"]["P0120019"].doubleValue
                    self.ppyMale50to54 = age_Male_50to54
                    self.ppyMale55to59 = age_Male_55to59
                    self.ppyMale60to64 = age_Male_60to61 + age_Male_62to64
                    self.dataAry.append(self.ppyMale50to54)
                    self.dataAry.append(self.ppyMale55to59)
                    self.dataAry.append(self.ppyMale60to64)
                    
                    // variable and for-in loop for male 50-64 total
                    var male_50to64 = 0.0
                    for index in 16...19{
                        male_50to64 += json["features"][0]["properties"]["P01200\(index)"].doubleValue
                    }
                    
                    // Females 50-64
                    let age_Female_50to54 = json["features"][0]["properties"]["P0120040"].doubleValue
                    let age_Female_55to59 = json["features"][0]["properties"]["P0120041"].doubleValue
                    let age_Female_60to61 = json["features"][0]["properties"]["P0120042"].doubleValue
                    let age_Female_62to64 = json["features"][0]["properties"]["P0120043"].doubleValue
                    self.ppyFemale50to54 = age_Female_50to54
                    self.ppyFemale55to59 = age_Female_55to59
                    self.ppyFemale60to64 = age_Female_60to61 + age_Female_62to64
                    self.dataAry.append(self.ppyFemale50to54)
                    self.dataAry.append(self.ppyFemale55to59)
                    self.dataAry.append(self.ppyFemale60to64)
                    
                    // variable and for-in loop for female 50-64 total
                    var female_50to64 = 0.0
                    for index in 40...43{
                        female_50to64 += json["features"][0]["properties"]["P01200\(index)"].doubleValue
                    }
                    
                    // Total 50-64
                    self.age_50to64 = Double(male_50to64 + female_50to64)
                    
                    // 18-64
                    self.age_18to64 = self.age_20to24 + self.age_25to34 + self.age_35to49 + self.age_50to64
                    
                    
                    Alamofire.request(request_url, method: .post, parameters: parameters2, encoding: JSONEncoding.default, headers: headers).responseJSON {
                        response in switch response.result {
                        case .success(let value):
                            let json = JSON(value)
                            //print(json)
                            
                            // Males 65 and over
                            let age_Male_65to66 = json["features"][0]["properties"]["P0120020"].doubleValue
                            let age_Male_67to69 = json["features"][0]["properties"]["P0120021"].doubleValue
                            let age_Male_70to74 = json["features"][0]["properties"]["P0120022"].doubleValue
                            let age_Male_75to79 = json["features"][0]["properties"]["P0120023"].doubleValue
                            let age_Male_80to84 = json["features"][0]["properties"]["P0120024"].doubleValue
                            let age_Male_Above85 = json["features"][0]["properties"]["P0120025"].doubleValue
                            self.ppyMale65to69 = age_Male_65to66 + age_Male_67to69
                            self.ppyMale70to74 = age_Male_70to74
                            self.ppyMale75to79 = age_Male_75to79
                            self.ppyMale80to84 = age_Male_80to84
                            self.ppyMaleAbove85 = age_Male_Above85
                            self.dataAry.append(self.ppyMale65to69)
                            self.dataAry.append(self.ppyMale70to74)
                            self.dataAry.append(self.ppyMale75to79)
                            self.dataAry.append(self.ppyMale80to84)
                            self.dataAry.append(self.ppyMaleAbove85)
                            
                            print("BUTTON M70-74: \(age_Male_70to74)")
                            print("BUTTON M75-79: \(age_Male_75to79)")
                            
                            // variable and for-in loop for male 50-64 total
                            var male_Above65 = 0.0
                            for index in 20...25{
                                male_Above65 += json["features"][0]["properties"]["P01200\(index)"].doubleValue
                            }
                            
                            // Females 50-64
                            let age_Female_65to66 = json["features"][0]["properties"]["P0120044"].doubleValue
                            let age_Female_67to69 = json["features"][0]["properties"]["P0120045"].doubleValue
                            let age_Female_70to74 = json["features"][0]["properties"]["P0120046"].doubleValue
                            let age_Female_75to79 = json["features"][0]["properties"]["P0120047"].doubleValue
                            let age_Female_80to84 = json["features"][0]["properties"]["P0120048"].doubleValue
                            let age_Female_Above85 = json["features"][0]["properties"]["P0120049"].doubleValue
                            self.ppyFemale65to69 = age_Female_65to66 + age_Female_67to69
                            self.ppyFemale70to74 = age_Female_70to74
                            self.ppyFemale75to79 = age_Female_75to79
                            self.ppyFemale80to84 = age_Female_80to84
                            self.ppyFemaleAbove85 = age_Female_Above85
                            self.dataAry.append(self.ppyFemale65to69)
                            self.dataAry.append(self.ppyFemale70to74)
                            self.dataAry.append(self.ppyFemale75to79)
                            self.dataAry.append(self.ppyFemale80to84)
                            self.dataAry.append(self.ppyFemaleAbove85)
                            
                            // variable and for-in loop for male 50-64 total
                            var female_Above65 = 0.0
                            for index in 44...49{
                                female_Above65 += json["features"][0]["properties"]["P01200\(index)"].doubleValue
                            }
                            // Total 65 and above
                            self.age_65Above = male_Above65 + female_Above65
                            
                            // Total Housing
                            self.housing_total = json["features"][0]["properties"]["H00010001"].doubleValue
                            // print("Total Housing    : \(housing_total)")
                            self.dataAry.append(self.housing_total)
                            
                            // Occupied
                            self.housing_Occupied = json["features"][0]["properties"]["H0180001"].doubleValue
                            //print("Housing Occupied : \(housing_Occupied)")
                            self.dataAry.append(self.housing_Occupied)
                            
                            // Owner Occupied
                            let housing_Owner_Occupied1 = json["features"][0]["properties"]["H0040002"].doubleValue
                            let housing_Owner_Occupied2 = json["features"][0]["properties"]["H0040003"].doubleValue
                            self.housing_Owner_OccupiedTOTAL = housing_Owner_Occupied1 + housing_Owner_Occupied2
                            self.dataAry.append(self.housing_Owner_OccupiedTOTAL)
                            
                            // Owner Population
                            let housing_Owner_Population1 = json["features"][0]["properties"]["H0110002"].doubleValue
                            let housing_Owner_Population2 = json["features"][0]["properties"]["H0110003"].doubleValue
                            self.housing_Owner_PopulationTOTAL = housing_Owner_Population1 + housing_Owner_Population2
                            self.dataAry.append(self.housing_Owner_PopulationTOTAL)
                            
                            // Renter Occupied
                            self.housing_Renter_Occupied = json["features"][0]["properties"]["H0040004"].doubleValue
                            self.dataAry.append(self.housing_Renter_Occupied)
                            
                            // Renter Population
                            self.housing_Renter_Population = json["features"][0]["properties"]["H0110004"].doubleValue
                            self.dataAry.append(self.housing_Renter_Population)
                            
                            // Housing with minors
                            let housing_withMinors1 = json["features"][0]["properties"]["H0190003"].doubleValue
                            let housing_withMinors2 = json["features"][0]["properties"]["H0190006"].doubleValue
                            self.housing_withMinorsTOTAL = housing_withMinors1 + housing_withMinors2
                            self.dataAry.append(self.housing_withMinorsTOTAL)
                            
                            // Vacant Housing
                            self.housing_Vacant = json["features"][0]["properties"]["H0050001"].doubleValue
                            self.dataAry.append(self.housing_Vacant)
                            
                            // Vacant Housing for rent
                            self.housing_Vacant_ForRent = json["features"][0]["properties"]["H0050002"].doubleValue
                            self.dataAry.append(self.housing_Vacant_ForRent)
                            
                            // Vacant Housing for sale
                            self.housing_Vacant_ForSale = json["features"][0]["properties"]["H0050004"].doubleValue
                            self.dataAry.append(self.housing_Vacant_ForSale)
                            
                            // Occasional use housing
                            self.housing_Seasonal = json["features"][0]["properties"]["H0050006"].doubleValue
                            self.dataAry.append(self.housing_Seasonal)
                            
                            // Census geography
                            self.countyVar = json["features"][0]["properties"]["COUNTY"].stringValue
                            self.tractVar = json["features"][0]["properties"]["NAME"].stringValue
                        case .failure(let error):
                            
                            print(error)
                        }
                    }
                case .failure(let error):
                    print(error)
                } // .result }
            } // responseJSON }
        } // if }
        else {
            print("No Location Found!")
        }
        
        defaults.set(dataAry, forKey: "save")
        tableView.reloadData()
    } // Buttion Function End
    
    @IBAction func saveBtnPressed(_ sender: UIBarButtonItem) {
        let fileName = "\(nearAddressVar) \(tractVar)"
        let DocumentDirURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let fileURL = DocumentDirURL.appendingPathComponent(fileName).appendingPathExtension("pdf")
        
        do {
            try PDFGenerator.generate(self.tableView, to: fileURL)
        } catch (let error) {
            print(error)
        }
    }
    
    @IBAction func fileBtnPressed(_ sender: UIBarButtonItem) {
        let fileExplorer = FileExplorerViewController()
        fileExplorer.delegate = self as? FileExplorerViewControllerDelegate
        self.present(fileExplorer, animated: true, completion:  nil)
    }
    
    // Location Function
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue: CLLocationCoordinate2D = (manager.location?.coordinate)! // change last for alt
        lastLocation = locValue
        let userLocation = locations[0]
        
        CLGeocoder().reverseGeocodeLocation(userLocation) { (placemarks, error) in
            
            // Check for errors
            if error != nil {
                
                print(error ?? "Unknown Error")
                
            } else {
                
                // Get the first placemark from the placemarks array.
                // This is your address object
                if let placemark = placemarks?[0] {
                    
                    // Create an empty string for street address
                    var streetAddress = ""
                    
                    // Check that values aren't nil, then add them to empty string
                    // "subThoroughfare" is building number, "thoroughfare" is street
                    if placemark.subThoroughfare != nil && placemark.thoroughfare != nil {
                        
                        streetAddress = placemark.subThoroughfare! + " " + placemark.thoroughfare!
                        self.addressVar = streetAddress
                        
                    } else {
                        
                        print("Unable to find street address")
                        
                    }
                    
                    // Same as above, but for city
                    var city = ""
                    
                    // locality gives you the city name
                    if placemark.locality != nil  {
                        
                        city = placemark.locality!
                        self.cityVar = city
                    } else {
                        
                        print("Unable to find city")
                        
                    }
                    
                    // Do the same for state
                    var state = ""
                    
                    // administrativeArea gives you the state
                    if placemark.administrativeArea != nil  {
                        
                        state = placemark.administrativeArea!
                        self.stateVar = state
                    } else {
                        
                        print("Unable to find state")
                        
                    }
                    
                    // And finally the postal code (zip code)
                    var zip = ""
                    
                    if placemark.postalCode != nil {
                        
                        zip = placemark.postalCode!
                        self.zipVar = zip
                        
                    } else {
                        
                        print("Unable to find zip")
                        
                    }
                    
                    let nearAddress: String = "\(city) \(state) \(zip)"
                    self.nearAddressVar = nearAddress
                    print("\(streetAddress)\n\(city), \(state) \(zip)")
                    
                }
                
            }
            
        }
        
    }
    
    // Displays the Table View
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 3900
    }
    // Controls the cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath) as! TableViewCell
        
        cell.themeNo = self.themeNo
        
        
        cell.lat = self.lat
        cell.lng = self.lng
        cell.alt = self.alt
        
        cell.addressVar = self.addressVar
        cell.nearAddressVar = self.nearAddressVar
        
        cell.stateVar = self.stateVar
        cell.countyVar = self.countyVar
        cell.tractVar = self.tractVar
        
        cell.totalPop = self.totalPop
        
        cell.ethnicity_HisOrLat = self.ethnicity_HisOrLat
        cell.ethnicity_Not_HisOrLat = self.ethnicity_Not_HisOrLat
        
        cell.white = self.race_white
        cell.africanAmerican = self.race_AfricanAmerican
        cell.asian = self.race_Asian
        cell.aian = self.race_AIAN
        cell.nhpi = self.race_NHPI
        cell.other = self.race_SOR
        cell.twoOrMore = self.race_ToMR
        
        cell.age_Male = self.age_Male
        cell.age_Female = self.age_Female
        cell.age_18Under = self.age_18Under
        cell.age_20to24 = self.age_20to24
        cell.age_25to34 = self.age_25to34
        cell.age_35to49 = self.age_35to49
        cell.age_50to64 = self.age_50to64
        cell.age_65Above = self.age_65Above
        cell.age_18to64 = self.age_18to64
        
        cell.housing_total = self.housing_total
        cell.housing_Occupied = self.housing_Occupied
        cell.housing_Owner_OccupiedTOTAL = self.housing_Owner_OccupiedTOTAL
        cell.housing_Owner_PopulationTOTAL = self.housing_Owner_PopulationTOTAL
        cell.housing_Renter_Occupied = self.housing_Renter_Occupied
        cell.housing_Renter_Population = self.housing_Renter_Population
        cell.housing_withMinorsTOTAL = housing_withMinorsTOTAL
        cell.housing_Vacant = self.housing_Vacant
        cell.housing_Vacant_ForRent = self.housing_Vacant_ForRent
        cell.housing_Vacant_ForSale = self.housing_Vacant_ForSale
        cell.housing_Seasonal = self.housing_Seasonal
        
        // Chart values
        cell.ppyMaleUnder5    = self.ppyMaleUnder5
        cell.ppyMale5to9      = self.ppyMale5to9
        cell.ppyMale10to14    = self.ppyMale10to14
        cell.ppyMale15to19    = self.ppyMale15to19
        cell.ppyMale20to24    = self.ppyMale20to24
        cell.ppyMale25to29    = self.ppyMale25to29
        cell.ppyMale30to34    = self.ppyMale30to34
        cell.ppyMale35to39    = self.ppyMale35to39
        cell.ppyMale40to44    = self.ppyMale40to44
        cell.ppyMale45to49    = self.ppyMale45to49
        cell.ppyMale50to54    = self.ppyMale50to54
        cell.ppyMale55to59    = self.ppyMale55to59
        cell.ppyMale60to64    = self.ppyMale65to69
        cell.ppyMale65to69    = self.ppyMale65to69
        cell.ppyMale70to74    = self.ppyMale70to74
        cell.ppyMale75to79    = self.ppyMale75to79
        cell.ppyMale80to84    = self.ppyMale80to84
        cell.ppyMaleAbove85   = self.ppyMaleAbove85
        cell.ppyFemaleUnder5  = self.ppyFemaleUnder5
        cell.ppyFemale5to9    = self.ppyFemale5to9
        cell.ppyFemale10to14  = self.ppyFemale10to14
        cell.ppyFemale15to19  = self.ppyFemale15to19
        cell.ppyFemale20to24  = self.ppyFemale20to24
        cell.ppyFemale25to29  = self.ppyFemale25to29
        cell.ppyFemale30to34  = self.ppyFemale30to34
        cell.ppyFemale35to39  = self.ppyFemale35to39
        cell.ppyFemale40to44  = self.ppyFemale40to44
        cell.ppyFemale45to49  = self.ppyFemale45to49
        cell.ppyFemale50to54  = self.ppyFemale50to54
        cell.ppyFemale55to59  = self.ppyFemale55to59
        cell.ppyFemale60to64  = self.ppyFemale60to64
        cell.ppyFemale65to69  = self.ppyFemale65to69
        cell.ppyFemale70to74  = self.ppyFemale70to74
        cell.ppyFemale75to79  = self.ppyFemale75to79
        cell.ppyFemale80to84  = self.ppyFemale80to84
        cell.ppyFemaleAbove85 = self.ppyFemaleAbove85
        
        cell.setMap()
        cell.setPyramidChart()
        cell.setGenderChart()
        cell.setEthnicityChart()
        cell.setRaceChart()
        cell.setHousingOccupiedChart()
        cell.setHousingOwnerOccupiedChart()
        cell.setHousingVacancyChart()
        
        print("S Table View F15-19: \(self.ppyFemale15to19)")
        print("S Table view M75-79: \(self.ppyMale75to79)")
        print("C Table View F15-19: \(cell.ppyFemale15to19)")
        print("C Table view M75-79: \(cell.ppyMale75to79)")
        
        return cell
    }
    
   
} // End of class


class TableViewCell: UITableViewCell {
    
    // used to get decimal places for doubles
    func round(_ value: Double, toDecimalPlaces places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (Darwin.round(value * divisor) / divisor)
    }
    
    var themeNo: Int = 0
    
    // Change the Label.text & Variables
    // coordinates
    
    // Chart Values
    var ppyMaleUnder5: Double = 0
    var ppyMale5to9: Double = 0
    var ppyMale10to14: Double = 0
    var ppyMale15to19: Double = 0
    var ppyMale20to24: Double = 0
    var ppyMale25to29: Double = 0
    var ppyMale30to34: Double = 0
    var ppyMale35to39: Double = 0
    var ppyMale40to44: Double = 0
    var ppyMale45to49: Double = 0
    var ppyMale50to54: Double = 0
    var ppyMale55to59: Double = 0
    var ppyMale60to64: Double = 0
    var ppyMale65to69: Double = 0
    var ppyMale70to74: Double = 0
    var ppyMale75to79: Double = 0
    var ppyMale80to84: Double = 0
    var ppyMaleAbove85: Double = 0
    var ppyFemaleUnder5: Double = 0
    var ppyFemale5to9: Double = 0
    var ppyFemale10to14: Double = 0
    var ppyFemale15to19: Double = 0
    var ppyFemale20to24: Double = 0
    var ppyFemale25to29: Double = 0
    var ppyFemale30to34: Double = 0
    var ppyFemale35to39: Double = 0
    var ppyFemale40to44: Double = 0
    var ppyFemale45to49: Double = 0
    var ppyFemale50to54: Double = 0
    var ppyFemale55to59: Double = 0
    var ppyFemale60to64: Double = 0
    var ppyFemale65to69: Double = 0
    var ppyFemale70to74: Double = 0
    var ppyFemale75to79: Double = 0
    var ppyFemale80to84: Double = 0
    var ppyFemaleAbove85: Double = 0
    
    // census variables
    var countyVar: String = "Test"
    
    // coordinates
    var lat: Double = 0 {
        didSet {
            latLabel.text =   "\(round(lat, toDecimalPlaces: 6))"
        }
    }
    var lng: Double = 0 {
        didSet {
            lngLabel.text = "\(round(lng, toDecimalPlaces: 6))"
        }
    }
    var alt: Double = 0 {
        didSet {
            altLabel.text = "\(alt)"
        }
    }
    // census geography
    var stateVar: String = "n/a" {
        didSet {
            censusGeoLabel.text = "State: \(stateVar) - County \(countyVar)"
        }
    }
    var tractVar: String = "n/a" {
        didSet {
            censusGeoLabel2.text = "\(tractVar)"
        }
    }
    // nearest address
    var addressVar: String = "n/a" {
        didSet {
            nearAddressLabel.text = "\(addressVar)"
        }
    }
    var nearAddressVar: String = "n/a" {
        didSet {
            nearAddressLabel2.text = "\(nearAddressVar)"
        }
    }
    //population
    var totalPop: Double = 0 {
        didSet {
            totalPopulationLabel.text = "\(Int(totalPop))"
        }
    }
    // ethnicity
    var ethnicity_HisOrLat: Double = 0 {
        didSet {
            hispanicLatinoLabel.text = "\(Int(ethnicity_HisOrLat))"
        }
    }
    var ethnicity_Not_HisOrLat: Double = 0 {
        didSet {
            nonHispanicLatinoLabel.text = "\(Int(ethnicity_Not_HisOrLat))"
        }
    }
    // race
    var white: Double = 0 {
        didSet {
            whiteLabel.text = "\(Int(white))"
        }
    }
    var africanAmerican: Double = 0 {
        didSet {
            africanAmericanLabel.text = "\(Int(africanAmerican))"
        }
    }
    var asian: Double = 0 {
        didSet {
            asianLabel.text = "\(Int(asian))"
        }
    }
    var aian: Double = 0 {
        didSet {
            aianLabel.text = "\(Int(aian))"
        }
    }
    var nhpi: Double = 0 {
        didSet {
            nhpiLabel.text = "\(Int(nhpi))"
        }
    }
    var other: Double = 0 {
        didSet {
            otherRaceLabel.text = "\(Int(other))"
        }
    }
    var twoOrMore: Double = 0 {
        didSet {
            twoPlusLabel.text = "\(Int(twoOrMore))"
        }
    }
    // age
    var age_Male: Double = 0 {
        didSet {
            maleLabel.text = "\(Int(age_Male))"
        }
    }
    var age_Female: Double = 0 {
        didSet {
            femaleLabel.text = "\(Int(age_Female))"
        }
    }
    var age_18Under: Double = 0 {
        didSet {
            under18Label.text = "\(Int(age_18Under))"
        }
    }
    var age_20to24: Double = 0 {
        didSet {
            ages20through24Label.text = "\(Int(age_20to24))"
        }
    }
    var age_25to34: Double = 0 {
        didSet {
            ages25through34Label.text = "\(Int(age_25to34))"
        }
    }
    var age_35to49: Double = 0 {
        didSet {
            ages35through49Label.text = "\(Int(age_35to49))"
        }
    }
    var age_50to64: Double = 0 {
        didSet {
            ages50through64Label.text = "\(Int(age_50to64))"
        }
    }
    var age_65Above: Double = 0 {
        didSet {
            ages65AndOverLabel.text = "\(Int(age_65Above))"
        }
    }
    var age_18to64: Double = 0 {
        didSet {
            ages18AndOverLabel.text = "\(Int(age_18to64))"
        }
    }
    
    // household
    var housing_total: Double = 0 {
        didSet {
            totalHousingLabel.text = "\(Int(housing_total))"
        }
    }
    var housing_Occupied: Double = 0 {
        didSet {
            occupiedLabel.text = "\(Int(housing_Occupied))"
        }
    }
    var housing_Owner_OccupiedTOTAL: Double = 0 {
        didSet {
            ownerOccupiedLabel.text = "\(Int(housing_Owner_OccupiedTOTAL))"
        }
    }
    var housing_Owner_PopulationTOTAL: Double = 0 {
        didSet {
            ownerOccupiedPopulationLabel.text = "\(Int(housing_Owner_PopulationTOTAL))"
        }
    }
    var housing_Renter_Occupied: Double = 0 {
        didSet {
            renterOccupiedLabel.text = "\(Int(housing_Renter_Occupied))"
        }
    }
    var housing_Renter_Population: Double = 0 {
        didSet {
            renterOccupiedPopulationLabel.text = "\(Int(housing_Renter_Population))"
        }
    }
    var housing_withMinorsTOTAL: Double = 0 {
        didSet {
            housingWithMinorsLabel.text = "\(Int(housing_withMinorsTOTAL))"
        }
    }
    var housing_Vacant: Double = 0 {
        didSet {
            vacantHousingLabel.text = "\(Int(housing_Vacant))"
        }
    }
    var housing_Vacant_ForRent: Double = 0 {
        didSet {
            forRentLabel.text = "\(Int(housing_Vacant_ForRent))"
        }
    }
    var housing_Vacant_ForSale: Double = 0 {
        didSet {
            forSaleLabel.text = "\(Int(housing_Vacant_ForSale))"
        }
    }
    var housing_Seasonal: Double = 0 {
        didSet {
            occasionalUseHousingLabel.text = "\(Int(housing_Seasonal))"
        }
    }
    
    class Annotation: NSObject, MKAnnotation {
        var title: String?
        var subtitle: String?
        var coordinate: CLLocationCoordinate2D
        init(coordinate: CLLocationCoordinate2D, title: String, subtitle: String) {
            self.title = title
            self.subtitle = subtitle
            self.coordinate = coordinate
        }
    }
    
    //@IBOutlet weak var mapImage: UIImageView!
    
    
    @IBOutlet var mapView: MKMapView!
    
    func setMap() {
        //let mapView = MKMapView()
        
        let latDelta: CLLocationDegrees = 0.03
        let lonDelta: CLLocationDegrees = 0.03
        let span = MKCoordinateSpan(latitudeDelta: latDelta, longitudeDelta: lonDelta)
        let coordinates = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        let region = MKCoordinateRegion(center: coordinates, span: span)
        let annotation = Annotation(coordinate: coordinates, title: "\(addressVar)", subtitle: "\(nearAddressVar)")
        mapView.addAnnotation(annotation)
        
        mapView.setRegion(region, animated: false)
        //mapView.frame.size.height = 300
        //mapView.frame.size.width = 359
        mapView.isScrollEnabled = false
        
        /*
        let options = MKMapSnapshotOptions()
        options.region = mapView.region
        options.size = mapView.frame.size
        options.scale = UIScreen.main.scale
        let snapshotter = MKMapSnapshotter(options: options)

        
        snapshotter.start { snapshot, error in
            guard let snapshot = snapshot else {
                print("Snapshot error")
                return
            }
            self.mapImage.image = snapshot.image
        }
        */
    }
    
    @IBOutlet var PyramidChart: HorizontalBarChartView!
    func setPyramidChart() {
        
        var yValues = [ChartDataEntry]()
        
        let yAxisFormatter = NumberFormatter()
        yAxisFormatter.negativePrefix = ""
        yAxisFormatter.positiveSuffix = "%"
        yAxisFormatter.negativeSuffix = "%"
        yAxisFormatter.minimumSignificantDigits = 1
        yAxisFormatter.minimumFractionDigits = 1
        
        PyramidChart.legend.horizontalAlignment = .center
        PyramidChart.chartDescription?.enabled = false
        PyramidChart.xAxis.drawGridLinesEnabled = false
        PyramidChart.xAxis.drawAxisLineEnabled = false
        PyramidChart.xAxis.labelCount = 18
        PyramidChart.leftAxis.enabled = false
        PyramidChart.rightAxis.drawGridLinesEnabled = false
        PyramidChart.pinchZoomEnabled = false
        PyramidChart.doubleTapToZoomEnabled = false
        PyramidChart.dragEnabled = false
        PyramidChart.highlightPerTapEnabled = false
        PyramidChart.fitBars = true
        PyramidChart.rightAxis.valueFormatter = DefaultAxisValueFormatter(formatter: yAxisFormatter)
        PyramidChart.animate(xAxisDuration: 1.0, yAxisDuration: 1.5)
        
        if themeNo == 1 {
            PyramidChart.backgroundColor = UIColor.black
            PyramidChart.xAxis.labelTextColor = UIColor.white
            PyramidChart.rightAxis.labelTextColor = UIColor.white
            PyramidChart.legend.textColor = UIColor.white
        }
        
        print("Chart F15-19: \(ppyFemale15to19)")
        print("Chart M70-74: \(ppyMale70to74)")
        print("Chart M75-79: \(ppyMale75to79)")
        
        yValues.append(BarChartDataEntry(x: 0, yValues: [-1 * ppyMaleUnder5 / totalPop * 100, ppyFemaleUnder5 / totalPop * 100]))
        yValues.append(BarChartDataEntry(x: 5, yValues: [-1 * ppyMale5to9 / totalPop * 100, ppyFemale5to9 / totalPop * 100]))
        yValues.append(BarChartDataEntry(x: 10, yValues: [-1 * ppyMale10to14 / totalPop * 100, ppyFemale10to14 / totalPop * 100]))
        yValues.append(BarChartDataEntry(x: 15, yValues: [-1 * ppyMale15to19 / totalPop * 100, ppyFemale15to19 / totalPop * 100]))
        yValues.append(BarChartDataEntry(x: 20, yValues: [-1 * ppyMale20to24 / totalPop * 100, ppyFemale20to24 / totalPop * 100]))
        yValues.append(BarChartDataEntry(x: 25, yValues: [-1 * ppyMale25to29 / totalPop * 100, ppyFemale25to29 / totalPop * 100]))
        yValues.append(BarChartDataEntry(x: 30, yValues: [-1 * ppyMale30to34 / totalPop * 100, ppyFemale30to34 / totalPop * 100]))
        yValues.append(BarChartDataEntry(x: 35, yValues: [-1 * ppyMale35to39 / totalPop * 100, ppyFemale35to39 / totalPop * 100]))
        yValues.append(BarChartDataEntry(x: 40, yValues: [-1 * ppyMale40to44 / totalPop * 100, ppyFemale40to44 / totalPop * 100]))
        yValues.append(BarChartDataEntry(x: 45, yValues: [-1 * ppyMale45to49 / totalPop * 100, ppyFemale45to49 / totalPop * 100]))
        yValues.append(BarChartDataEntry(x: 50, yValues: [-1 * ppyMale50to54 / totalPop * 100, ppyFemale50to54 / totalPop * 100]))
        yValues.append(BarChartDataEntry(x: 55, yValues: [-1 * ppyMale55to59 / totalPop * 100, ppyFemale55to59 / totalPop * 100]))
        yValues.append(BarChartDataEntry(x: 60, yValues: [-1 * ppyMale60to64 / totalPop * 100, ppyFemale60to64 / totalPop * 100]))
        yValues.append(BarChartDataEntry(x: 65, yValues: [-1 * ppyMale65to69 / totalPop * 100, ppyFemale65to69 / totalPop * 100]))
        yValues.append(BarChartDataEntry(x: 70, yValues: [-1 * ppyMale70to74 / totalPop * 100, ppyFemale70to74 / totalPop * 100]))
        yValues.append(BarChartDataEntry(x: 75, yValues: [-1 * ppyMale75to79 / totalPop * 100, ppyFemale75to79 / totalPop * 100]))
        yValues.append(BarChartDataEntry(x: 80, yValues: [-1 * ppyMale80to84 / totalPop * 100, ppyFemale80to84 / totalPop * 100]))
        yValues.append(BarChartDataEntry(x: 85, yValues: [-1 * ppyMaleAbove85 / totalPop * 100, ppyFemaleAbove85 / totalPop * 100]))
        
        let chartDataSet = BarChartDataSet(values: yValues, label: "")
        chartDataSet.drawValuesEnabled = false
        chartDataSet.colors =
            [UIColor(
                red: CGFloat(102.0 / 255.0),
                green: CGFloat(179.0 / 255.0),
                blue: CGFloat(255.0 / 255.0),
                alpha: CGFloat(1.0)),
             UIColor(
                red: CGFloat(255.0 / 255.0),
                green: CGFloat(153.0 / 255.0),
                blue: CGFloat(153.0 / 255.0),
                alpha: CGFloat(1.0))]
        
        chartDataSet.stackLabels = ["Male", "Female"]
        
        let chartData = BarChartData(dataSet: chartDataSet)
        
        chartData.barWidth = 4.5
        
        PyramidChart.data = chartData
    }
    
    @IBOutlet var GenderChart: PieChartView!
    func setGenderChart() {
        
        GenderChart.legend.enabled = false
        GenderChart.chartDescription?.enabled = false
        GenderChart.drawHoleEnabled = false
        GenderChart.animate(xAxisDuration: 1.0)
        GenderChart.animate(yAxisDuration: 1.0)
        
        
        if themeNo == 1 {
            GenderChart.backgroundColor = UIColor.black
        }
        
        let chartDataSet = PieChartDataSet(values: [PieChartDataEntry(value: age_Male / totalPop, label: "Male"), PieChartDataEntry(value: age_Female / totalPop, label: "Female")], label: "")
        chartDataSet.colors =
            [UIColor(
                red: CGFloat(102.0 / 255.0),
                green: CGFloat(179.0 / 255.0),
                blue: CGFloat(255.0 / 255.0),
                alpha: CGFloat(1.0)),
             UIColor(
                red: CGFloat(255.0 / 255.0),
                green: CGFloat(153.0 / 255.0),
                blue: CGFloat(153.0 / 255.0),
                alpha: CGFloat(1.0))]
        
        let chartData = PieChartData(dataSet: chartDataSet)
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        formatter.maximumFractionDigits = 1
        formatter.multiplier = 100.0
        chartData.setValueFormatter(DefaultValueFormatter(formatter:formatter))
        GenderChart.data = chartData
    }
    
    @IBOutlet var EthnicityChart: PieChartView!
    func setEthnicityChart() {
        
        EthnicityChart.legend.horizontalAlignment = .center
        EthnicityChart.chartDescription?.enabled = false
        EthnicityChart.drawHoleEnabled = false
        EthnicityChart.drawEntryLabelsEnabled = false
        EthnicityChart.animate(yAxisDuration: 1.0)
        EthnicityChart.animate(xAxisDuration: 1.0)
        
        if themeNo == 1 {
            EthnicityChart.backgroundColor = UIColor.black
            EthnicityChart.legend.textColor = UIColor.white
        }
        
        let chartDataSet = PieChartDataSet(values: [PieChartDataEntry(value: ethnicity_HisOrLat / totalPop, label: "Hispanic or Latino"), PieChartDataEntry(value: ethnicity_Not_HisOrLat / totalPop, label: "Not Hispanic or Latino")], label: "")
        chartDataSet.colors =
            [UIColor(
                red: CGFloat(0.0 / 255.0),
                green: CGFloat(104.0 / 255.0),
                blue: CGFloat(71.0 / 255.0),
                alpha: CGFloat(1.0)),
             UIColor(
                red: CGFloat(206.0 / 255.0),
                green: CGFloat(17.0 / 255.0),
                blue: CGFloat(38.0 / 255.0),
                alpha: CGFloat(1.0))]
        
        let chartData = PieChartData(dataSet: chartDataSet)
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        formatter.maximumFractionDigits = 1
        formatter.multiplier = 100.0
        chartData.setValueFormatter(DefaultValueFormatter(formatter:formatter))
        EthnicityChart.data = chartData
    }
    
    @IBOutlet var RaceChart: PieChartView!
    func setRaceChart() {
        
        var yValues = [ChartDataEntry]()
        
        RaceChart.legend.horizontalAlignment = .center
        RaceChart.chartDescription?.enabled = false
        RaceChart.drawHoleEnabled = false
        RaceChart.drawEntryLabelsEnabled = false
        RaceChart.animate(xAxisDuration: 1.0)
        RaceChart.animate(yAxisDuration: 1.0)
        
        if themeNo == 1 {
            RaceChart.backgroundColor = UIColor.black
            RaceChart.legend.textColor = UIColor.white
        }
        
        yValues.append(PieChartDataEntry(value: white / totalPop, label: "White"))
        yValues.append(PieChartDataEntry(value: africanAmerican / totalPop, label: "African American"))
        yValues.append(PieChartDataEntry(value: asian / totalPop, label: "Asian"))
        yValues.append(PieChartDataEntry(value: aian / totalPop, label: "AIAN"))
        yValues.append(PieChartDataEntry(value: nhpi / totalPop, label: "NHPI"))
        yValues.append(PieChartDataEntry(value: other / totalPop, label: "Other"))
        yValues.append(PieChartDataEntry(value: twoOrMore / totalPop, label: "Two or More"))
        
        let chartDataSet = PieChartDataSet(values: yValues, label: "")
        chartDataSet.colors =
            [UIColor(
                red: CGFloat(204.0 / 255.0),
                green: CGFloat(255.0 / 255.0),
                blue: CGFloat(255.0 / 255.0),
                alpha: CGFloat(1.0)),
             UIColor(
                red: CGFloat(51.0 / 255.0),
                green: CGFloat(153.0 / 255.0),
                blue: CGFloat(255.0 / 255.0),
                alpha: CGFloat(1.0)),
             UIColor(
                red: CGFloat(0.0 / 255.0),
                green: CGFloat(153.0 / 255.0),
                blue: CGFloat(51.0 / 255.0),
                alpha: CGFloat(1.0)),
             UIColor(
                red: CGFloat(255.0 / 255.0),
                green: CGFloat(255.0 / 255.0),
                blue: CGFloat(0.0 / 255.0),
                alpha: CGFloat(1.0)),
             UIColor(
                red: CGFloat(255.0 / 255.0),
                green: CGFloat(153.0 / 255.0),
                blue: CGFloat(51.0 / 255.0),
                alpha: CGFloat(1.0)),
             UIColor(
                red: CGFloat(128.0 / 255.0),
                green: CGFloat(0.0 / 255.0),
                blue: CGFloat(0.0 / 255.0),
                alpha: CGFloat(1.0)),
             UIColor(
                red: CGFloat(204.0 / 255.0),
                green: CGFloat(0.0 / 255.0),
                blue: CGFloat(0.0 / 255.0),
                alpha: CGFloat(1.0))]
        
        chartDataSet.drawValuesEnabled = false
        let chartData = PieChartData(dataSet: chartDataSet)
        
        RaceChart.data = chartData
    }
    
    @IBOutlet var HousingOccupiedChart: PieChartView!
    func setHousingOccupiedChart() {
        
        HousingOccupiedChart.legend.enabled = false
        HousingOccupiedChart.chartDescription?.enabled = false
        HousingOccupiedChart.drawHoleEnabled = false
        HousingOccupiedChart.animate(yAxisDuration: 1.0)
        HousingOccupiedChart.animate(xAxisDuration: 1.0)
        
        if themeNo == 1 {
            HousingOccupiedChart.backgroundColor = UIColor.black
        }
        
        let chartDataSet = PieChartDataSet(values: [PieChartDataEntry(value: housing_Occupied / housing_total, label: "Occupied"), PieChartDataEntry(value: housing_Vacant / housing_total, label: "Vacant")], label: "")
        chartDataSet.colors =
            [UIColor(
                red: CGFloat(230.0 / 255.0),
                green: CGFloat(230.0 / 255.0),
                blue: CGFloat(0.0 / 255.0),
                alpha: CGFloat(1.0)),
             UIColor(
                red: CGFloat(128.0 / 255.0),
                green: CGFloat(128.0 / 255.0),
                blue: CGFloat(0.0 / 255.0),
                alpha: CGFloat(1.0))]
        
        let chartData = PieChartData(dataSet: chartDataSet)
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        formatter.maximumFractionDigits = 1
        formatter.multiplier = 100.0
        chartData.setValueFormatter(DefaultValueFormatter(formatter:formatter))
        
        HousingOccupiedChart.data = chartData
    }
    
    @IBOutlet var HousingOwnerOccupiedChart: PieChartView!
    func setHousingOwnerOccupiedChart() {
        
        HousingOwnerOccupiedChart.legend.enabled = false
        HousingOwnerOccupiedChart.chartDescription?.enabled = false
        HousingOwnerOccupiedChart.drawHoleEnabled = false
        HousingOwnerOccupiedChart.animate(yAxisDuration: 1.0)
        HousingOwnerOccupiedChart.animate(xAxisDuration: 1.0)
        
        if themeNo == 1 {
            HousingOwnerOccupiedChart.backgroundColor = UIColor.black
        }
        
        let chartDataSet = PieChartDataSet(values: [PieChartDataEntry(value: housing_Owner_OccupiedTOTAL / housing_Occupied, label: "Owner Occupied"), PieChartDataEntry(value: housing_Renter_Occupied / housing_Occupied, label: "Renter Occupied")], label: "")
        chartDataSet.colors =
            [UIColor(
                red: CGFloat(0.0 / 255.0),
                green: CGFloat(204.0 / 255.0),
                blue: CGFloat(204.0 / 255.0),
                alpha: CGFloat(1.0)),
             UIColor(
                red: CGFloat(0.0 / 255.0),
                green: CGFloat(128.0 / 255.0),
                blue: CGFloat(128.0 / 255.0),
                alpha: CGFloat(1.0))]
        
        let chartData = PieChartData(dataSet: chartDataSet)
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        formatter.maximumFractionDigits = 1
        formatter.multiplier = 100.0
        chartData.setValueFormatter(DefaultValueFormatter(formatter:formatter))
        
        HousingOwnerOccupiedChart.data = chartData
    }
    
    @IBOutlet var HousingVacancyChart: PieChartView!
    func setHousingVacancyChart() {
        HousingVacancyChart.legend.enabled = false
        HousingVacancyChart.chartDescription?.enabled = false
        HousingVacancyChart.drawHoleEnabled = false
        HousingVacancyChart.animate(xAxisDuration: 1.0)
        HousingVacancyChart.animate(yAxisDuration: 1.0)
        
        if themeNo == 1 {
            HousingVacancyChart.backgroundColor = UIColor.black
        }
        
        let chartDataSet = PieChartDataSet(values: [PieChartDataEntry(value: housing_Vacant_ForRent / housing_Vacant, label: "For Rent"), PieChartDataEntry(value: housing_Vacant_ForSale / housing_Vacant, label: "For Sale")], label: "")
        chartDataSet.colors =
            [UIColor(
                red: CGFloat(255.0 / 255.0),
                green: CGFloat(153.0 / 255.0),
                blue: CGFloat(0.0 / 255.0),
                alpha: CGFloat(1.0)),
             UIColor(
                red: CGFloat(204.0 / 255.0),
                green: CGFloat(122.0 / 255.0),
                blue: CGFloat(0.0 / 255.0),
                alpha: CGFloat(1.0))]
        
        let chartData = PieChartData(dataSet: chartDataSet)
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        formatter.maximumFractionDigits = 1
        formatter.multiplier = 100.0
        chartData.setValueFormatter(DefaultValueFormatter(formatter: formatter))
        
        HousingVacancyChart.data = chartData
    }
    
    // Outlets
    @IBOutlet weak var latLabel: UILabel!
    @IBOutlet weak var lngLabel: UILabel!
    @IBOutlet weak var altLabel: UILabel!
    @IBOutlet weak var censusGeoLabel: UILabel!
    @IBOutlet weak var censusGeoLabel2: UILabel!
    @IBOutlet weak var nearAddressLabel: UILabel!
    @IBOutlet weak var nearAddressLabel2: UILabel!
    @IBOutlet weak var totalPopulationLabel: UILabel!
    @IBOutlet weak var hispanicLatinoLabel: UILabel!
    @IBOutlet weak var nonHispanicLatinoLabel: UILabel!
    @IBOutlet weak var whiteLabel: UILabel!
    @IBOutlet weak var africanAmericanLabel: UILabel!
    @IBOutlet weak var asianLabel: UILabel!
    @IBOutlet weak var aianLabel: UILabel!
    @IBOutlet weak var nhpiLabel: UILabel!
    @IBOutlet weak var otherRaceLabel: UILabel!
    @IBOutlet weak var twoPlusLabel: UILabel!
    @IBOutlet weak var maleLabel: UILabel!
    @IBOutlet weak var femaleLabel: UILabel!
    @IBOutlet weak var under18Label: UILabel!
    @IBOutlet weak var ages18AndOverLabel: UILabel!
    @IBOutlet weak var ages20through24Label: UILabel!
    @IBOutlet weak var ages25through34Label: UILabel!
    @IBOutlet weak var ages35through49Label: UILabel!
    @IBOutlet weak var ages50through64Label: UILabel!
    @IBOutlet weak var ages65AndOverLabel: UILabel!
    @IBOutlet weak var totalHousingLabel: UILabel!
    @IBOutlet weak var occupiedLabel: UILabel!
    @IBOutlet weak var ownerOccupiedLabel: UILabel!
    @IBOutlet weak var ownerOccupiedPopulationLabel: UILabel!
    @IBOutlet weak var renterOccupiedLabel: UILabel!
    @IBOutlet weak var renterOccupiedPopulationLabel: UILabel!
    @IBOutlet weak var housingWithMinorsLabel: UILabel!
    @IBOutlet weak var vacantHousingLabel: UILabel!
    @IBOutlet weak var forRentLabel: UILabel!
    @IBOutlet weak var forSaleLabel: UILabel!
    @IBOutlet weak var occasionalUseHousingLabel: UILabel!
}













